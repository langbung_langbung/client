<?php

namespace App\Http\Controllers;
// use GuzzleHttp\Client;
use Illuminate\Http\Request;
// use GuzzleHttp\Exception\GuzzleException;
use Session;
use GuzzleHttp\Client;

class OAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->name;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAuthurizationCode(){
        $query = http_build_query([
            'client_id'     => '6',
            'redirect_uri'  => 'http://127.0.0.1:2222/callback',
            'response_type' => 'code',
            'scope'         => '',
        ]);
        
        return redirect('http://127.0.0.1:1111/oauth/authorize?'.$query);
    }
    public function getImplicit() {
        $query = http_build_query([
            'client_id' => 'client-id',
            'redirect_uri' => 'http://127.0.0.1:2222/callback',
            'response_type' => 'token',
            'scope' => '',
        ]);
    
        return redirect('http://your-app.com/oauth/authorize?'.$query);
    }

    public function getToken(Request $request) {
        $http = new Client();
        // $http = new GuzzleHttp\Client;
        if (request('code')) {
            try{
                $response = $http->request('POST', 'http://127.0.0.1:1111/oauth/token', [
                    'form_params' => [
                        'grant_type'    => 'authorization_code',
                        'client_id'     => '6',
                        'client_secret' => 'MiMM0gnLscC5icdpjlPPBt8J7Z0iDuH2BAjV8Men',
                        'redirect_uri'  => 'http://127.0.0.1:2222/callback',
                        'code'          => request('code'),
                    ],
                ]);
                $respond = json_decode($response->getBody(),TRUE);
                $token = $respond["access_token"];
                // return json_decode((string)$response->getBody(), TRUE);
                // return $respond;
                $head="access token";
                Session::put('access_token',$token);
                return view("meHome",compact("token","head"));
            }catch(Exception $e){
                // return array((string)response()->json(['error' => request('error')]));
                return "error token";
            }
            
        } else {
            // return array(response()->json(['error' => request('error')]));
            return "error token";
        }
    }

    private function checkToken($respond){
        if(array_key_exists("access_token",$respond)){
            return true;
        }else{
            return false;
        }
    }



    public function getTodos(){
        // require "vendor/autoload.php";

        $http = new Client;
            $response = $http->get('http://127.0.0.1:1111/api/todos', [
                'headers' => [
                    'Authorization' => 'Bearer '.Session::get("access_token"),
                ]
            ]);
        
            $todos = json_decode( (string) $response->getBody() );
        
            $todoList = "";
            foreach ($todos as $todo) {
                $todoList .= "<li>{$todo->task}".($todo->done ? '✅' : '')."</li>";
            }
        
            echo "<h1>Api todos</h1><ul>{$todoList}</ul>";
        
        // } catch (GuzzleHttp\Exception\BadResponseException $e) {
        //     echo "Unable to retrieve access token.";
        // }
    }

    public function checkSession($key){
        if(Session::has($key))
            echo $key." : ".Session::get($key);
        else
            echo "no '".$key."' in session";
    }
    
    
}

