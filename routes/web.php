<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// grant type
Route::get('/redirect',"OAuthController@getAuthurizationCode" )->name("get.token");
Route::get('/callback',"OAuthController@getToken");
// Route::get('/redirect',);

Route::get('/api/todos',"OAuthController@getTodos")->name("get.todos");

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('check/{key}', "OAuthController@checkSession");
